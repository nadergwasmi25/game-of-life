package fr.sdv.ngo.automates;

public interface Cell {

    Cell newGeneration(int nbNeighbours);

    String getAsString();

    Boolean isAlive();
}

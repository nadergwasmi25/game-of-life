package fr.sdv.ngo.automates;

public class DeadCell implements Cell {

    @Override
    public Cell newGeneration(int nbNeighbours) {
        return null;
    }

    @Override
    public String getAsString() {
        return "0";
    }

    @Override
    public Boolean isAlive() {
        return false;
    }

}

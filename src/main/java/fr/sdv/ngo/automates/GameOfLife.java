package fr.sdv.ngo.automates;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class GameOfLife {

    public static void main(String[] args) throws InterruptedException {


            System.out.print("Number of iterations : ");
            Scanner in = new Scanner(System.in);
            int iterations = in.nextInt();
            in.close();
            World world = new World(6, 6);
            world.generateWorld();
            for (int i = 0; i<iterations; i++){
                world.nextGeneration();
                world.displayWorld();
                System.out.println();
            }





    }

}

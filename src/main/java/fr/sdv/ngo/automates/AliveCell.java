package fr.sdv.ngo.automates;

public class AliveCell implements Cell {

    @Override
    public Cell newGeneration(int nbNeighbours) {

        return null;
    }

    @Override
    public String getAsString() {
        return "1";
    }

    @Override
    public Boolean isAlive() {
        return true;
    }

}

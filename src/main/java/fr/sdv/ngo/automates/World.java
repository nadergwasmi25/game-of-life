package fr.sdv.ngo.automates;

import fr.sdv.ngo.automates.utils.Constant;

import java.util.Random;

/**
 *  Class for the world of game of life
 *
 */
public class World {

    private int[][] grid;
    private Integer nbColumn;
    private Integer nbRow;

    public World(Integer nbRow, Integer nbColumn) {
        this.nbColumn = nbColumn;
        this.nbRow = nbRow;
        this.grid = new int[this.nbRow][this.nbColumn];

    }

    /**
     *  GETTER AND SETTER
     */
    public Integer getNbColumn() {
        return nbColumn;
    }

    public void setNbColumn(Integer nbColumn) {
        this.nbColumn = nbColumn;
    }

    public Integer getNbRow() {
        return nbRow;
    }

    public void setNbRow(Integer nbRow) {
        this.nbRow = nbRow;
    }

    public int getCell(int row, int col) {
        return grid[row][col];
    }

    public void setCell(int row, int col, int status) {
        grid[row][col] = status;
    }

    public int[][] getGrid() {
      return grid;
    }

    public void setGrid(int[][] grid) {
      this.grid = grid;
  }


  /**
   * method for generate a random world with 0 and 1
   */
    public void generateWorld() {
        for (int i = 0; i < nbRow; i++) {
            for (int j = 0; j < nbColumn; j++) {
                int cellRandom = getRandomCell();
                grid[i][j] = cellRandom;
            }
        }
        displayWorld();
        System.out.println();
    }

    /**
     *  generate a random value for a cell 0 or 1
     */
    public int getRandomCell() {
        return new Random().nextBoolean()? 1 : 0;
    }

    /**
     * count how many neighbours have a cell
     */
    public int countNeighbours(int row, int column) {
        int i, j, count = 0;
        for (i = row - 1; i <= row + 1; i++) {
            for (j = column - 1; j <= column + 1; j++) {
                if (
                    (i == row && j == column) ||
                    (i < 0 || j < 0) ||
                    (i >= nbRow || j >= nbColumn)
                ) {
                    continue;
                }
                if (grid[i][j] == 1) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * update world with the next generation
     */
    void nextGeneration() {
        int[][] firstGrid = new int[this.nbRow][this.nbRow];
        for (int row = 0; row < this.nbRow; row++) {
            for (int col = 0; col < this.nbColumn; col++) {
                int neighbours = countNeighbours(row, col);
                int status = Constant.DEAD_CELL;
                if (neighbours == 2 || neighbours == 3) {
                    status = Constant.ALIVE_CELL;
                }
                firstGrid[row][col] = status;
            }
        }
        grid = firstGrid;
    }

    /**
     * method for display world grid cell by cell
     */
    void displayWorld() {
        for (int i = 0; i < nbRow; i++) {
            for (int j = 0; j < nbColumn; j++) {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
    }

}

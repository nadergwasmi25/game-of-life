package fr.sdv.ngo.automates;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;


public class WorldTest {

  World theWorld;
  int[][] world;

  @Before
  public void setup() {
    world = new int[][]{
      {0, 1, 1, 0, 0, 1},
      {0, 1, 1, 0, 0, 1},
      {0, 0, 1, 0, 1, 0},
      {0, 0, 0, 0, 0, 1},
      {1, 1, 1, 0, 1, 1},
      {0, 0, 0, 1, 0, 1}
    };
    theWorld = new World(6, 6);
    theWorld.setGrid(world);
  }

  @Test
  public void testWorldGeneration_rows(){
    theWorld.generateWorld();
    int nbRow = theWorld.getNbRow();
    assertEquals(6, nbRow);
  }

  @Test
  public void testWorldGeneration_colums(){
    theWorld.generateWorld();
    int nbCol = theWorld.getNbColumn();
    assertEquals(6, nbCol);
  }

  @Test
  public void testCountNeighbours() {

    int nbNeighbour = theWorld.countNeighbours(5, 1);

    assertEquals(3, nbNeighbour);

  }


  @Test
  public void testNextGeneration() {
    int[][] worldNextGeneration = new int[][]{
      {1, 1, 1, 1, 1, 0},
      {1, 0, 0, 0, 1, 1},
      {0, 1, 1, 1, 1, 1},
      {1, 0, 1, 0, 0, 1},
      {0, 1, 1, 1, 0, 1},
      {1, 1, 1, 1, 0, 1}

    };

    theWorld.nextGeneration();
    assertArrayEquals(worldNextGeneration, theWorld.getGrid());

  }




}

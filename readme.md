Game of Life
============
This is a simple implementation of Conway's Game of Life in JAVA. 

The program is a simple console application that will run the game for a specified number of iterations 
and print the final states to the console.


Usage
-----
To run the program, simply go to the following class "GameOfLife":

1. Set the number of rows and collumns for World.
```
World world = new World(6, 6);
``` 
2. Select the number of iterations for the game.
3. Press Enter to start the game.

Tests
-----
To run the tests simply run the following Class "WorldTest".

Prerequisites
-------------
* Java 11



